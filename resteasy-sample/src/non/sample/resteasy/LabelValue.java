/**
 * 
 */
package non.sample.resteasy;

import java.io.Serializable;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 * @author Shawn Johnson
 *
 */
@XmlJavaTypeAdapter(LabelValueAdapter.class)
public class LabelValue implements Serializable {

	private static final long serialVersionUID = -8261786952662965310L;

	private String label;
	private String value;
	
	LabelValue() {
		// this breaks the rpc in dev mode, made constructor less accessibile instead
		// assert false : "Empty constructor for Serialization only";
	}
	
	public LabelValue(String label, String value) {
		this.label = label;
		this.value = value;
	}
	
	
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
}
