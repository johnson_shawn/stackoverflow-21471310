/**
 * 
 */
package non.sample.resteasy;


import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

@Path("/test")
public class TestService  {

	
	@GET
	@Produces(value = { "application/json", "application/xml" })
	public TestModel getModel() {
			TestModel docket = getSampleModel();
			return docket;

	}
	
	
	public static void main(String[] args) throws Exception {
        JAXBContext jc = JAXBContext.newInstance(TestModel.class, LabelValue.class);
        LabelValueAdapter adapter = new LabelValueAdapter(jc);
        
        TestModel docket = getSampleModel();
 
        Marshaller marshaller = jc.createMarshaller();
        marshaller.setAdapter(adapter);
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(docket, System.out);
    }


	private static TestModel getSampleModel() {
		TestModel model = new TestModel();
        List<LabelValue> metadata = new ArrayList<LabelValue>();
        metadata.add(new LabelValue("Distilled Water", "Deer Park"));
        metadata.add(new LabelValue("Mineral Water", "Mountain Stream"));
        metadata.add(new LabelValue("Purified Water", "Mr. Water"));
        model.setMetadata(metadata);
		return model;
	}
	
}
