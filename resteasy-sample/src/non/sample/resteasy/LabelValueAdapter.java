package non.sample.resteasy;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
 
public class LabelValueAdapter extends XmlAdapter<Element, LabelValue> {
 
    private ClassLoader classLoader;
    private DocumentBuilder documentBuilder;
    private JAXBContext jaxbContext;
 
    public LabelValueAdapter() {
        classLoader = Thread.currentThread().getContextClassLoader();
    }
 
    public LabelValueAdapter(JAXBContext jaxbContext) {
        this();
        this.jaxbContext = jaxbContext;
    }
 
    private DocumentBuilder getDocumentBuilder() throws Exception {
        // Lazy load the DocumentBuilder as it is not used for unmarshalling.
        if (null == documentBuilder) {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            documentBuilder = dbf.newDocumentBuilder();
        }
        return documentBuilder;
    }
 
    private JAXBContext getJAXBContext(Class<?> type) throws Exception {
        if (null == jaxbContext) {
            // A JAXBContext was not set, so create a new one based  on the type.
            return JAXBContext.newInstance(type);
        }
        return jaxbContext;
    }
 
    @Override
    public Element marshal(LabelValue labelValueModel) throws Exception {
        if (null == labelValueModel) {
            return null;
        }
 
        // 1. Build the JAXBElement to wrap the instance of Parameter.
        String elementName = getCleanLabel(labelValueModel.getLabel());
        QName rootElement = new QName(elementName);
        String value = labelValueModel.getValue();
//        Class<?> type = value.getClass();
        JAXBElement<String> jaxbElement = new JAXBElement<String>(rootElement, String.class, value);
 
        // 2.  Marshal the JAXBElement to a DOM element.
        Document document = getDocumentBuilder().newDocument();
        Marshaller marshaller = getJAXBContext(String.class).createMarshaller();
        marshaller.marshal(jaxbElement, document);
        Element element = document.getDocumentElement();
 
        // 3.  Set the type attribute based on the value's type.
//        element.setAttribute("type", type.getName());
        element.setAttribute("label", labelValueModel.getLabel());
        return element;
    }
 
    @Override
    public LabelValue unmarshal(Element element) throws Exception {
        throw new UnsupportedOperationException();
    }
    
	private String getCleanLabel(String attributeLabel) {
		attributeLabel = attributeLabel.replaceAll("dk_", "").replaceAll("[()]", "")
				.replaceAll("[^\\w\\s]", "_").replaceAll(" ", "_")
				.toUpperCase();
//		attributeLabel = CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL,
//				attributeLabel);
		return attributeLabel;
	}

 
}
