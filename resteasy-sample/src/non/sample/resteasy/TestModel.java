package non.sample.resteasy;

import java.util.List;

import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public class TestModel {
	private List<LabelValue> metadata;

 
    @XmlAnyElement
    public List<LabelValue> getMetadata() {
        return metadata;
    }
 
    public void setMetadata(List<LabelValue> metadata) {
        this.metadata = metadata;
    }
}
